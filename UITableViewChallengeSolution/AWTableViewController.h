//
//  AWTableViewController.h
//  UITableViewChallengeSolution
//
//  Created by Adrian Watzlawczyk on 05/09/14.
//  Copyright (c) 2014 ___ADRIANWATZLAWCZYK___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWAddSpaceObjectViewController.h"

@interface AWTableViewController : UITableViewController <AWAddSpaceObjectViewControllerDelegate>

@property (strong,nonatomic)NSMutableArray *planets;
@property (strong,nonatomic)NSMutableArray *addedSpaceObjects;

@end
