//
//  AWTableViewController.m
//  UITableViewChallengeSolution
//
//  Created by Adrian Watzlawczyk on 05/09/14.
//  Copyright (c) 2014 ___ADRIANWATZLAWCZYK___. All rights reserved.
//

#import "AWTableViewController.h"
#import "AstronomicalData.h"
#import "AWSpaceObject.h"
#import "AWSpaceImageViewController.h"
#import "AWSpaceDataViewController.h"

@interface AWTableViewController ()

@end

@implementation AWTableViewController

#define ADDED_SPACE_OBJECT_KEY @"Added Space Objects Array"

#pragma mark - Lazy instantiation of Properties

-(NSMutableArray *)addedSpaceObjects
{
    if (!_addedSpaceObjects) {
        _addedSpaceObjects = [[NSMutableArray alloc]init];
    }
    return _addedSpaceObjects;
}

-(NSMutableArray *)planets
{
    if (!_planets) {
        _planets = [[NSMutableArray alloc] init];
    }
    return _planets;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    for (NSMutableDictionary *planetData in [AstronomicalData allKnownPlanets]) {
        NSString *imageName = [NSString stringWithFormat:@"%@.jpg",planetData[PLANET_NAME] ];
        AWSpaceObject *planet = [[AWSpaceObject alloc] initWithData:planetData andImage:[UIImage imageNamed:imageName]];
        [self.planets addObject:planet];
    }
    
    
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*The prepareForSegue method is called right before the view controller
     transition occurs. Notice that we do introspection to ensure that the segue 
     is being triggered by the UITableViewCell. We then confirm that 
     destination ViewController is the AWSpaceImageViewController.
     Finally, we create a variable named nextViewController that points to
     our destination ViewController. Determine the indexPath of the selected cell and 
     use that indexPath to acces aAWSpaceObject in our planet array. Finally set the
     property spaceObject of the variable nextViewController equal to
     the selected object. */
    
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        if ([segue.destinationViewController isKindOfClass:[AWSpaceImageViewController class]])
        {
            AWSpaceImageViewController *nextViewController = segue.destinationViewController;
            NSIndexPath *path = [self.tableView indexPathForCell:sender];
            AWSpaceObject *selectedObject;
            
            if (path.section == 0) {
                selectedObject = self.planets[path.row];
            }
            else if (path.section == 1)
            {
                selectedObject = self.addedSpaceObjects[path.row];
            }
            nextViewController.spaceObject = selectedObject;
        }
    }
    if ([sender isKindOfClass:[NSIndexPath class]]) {
        if ([segue.destinationViewController isKindOfClass:[AWSpaceDataViewController class] ]) {
            AWSpaceDataViewController *targetViewController = segue.destinationViewController;
            NSIndexPath *path = sender;
            AWSpaceObject *selectedObject;
            
            if (path.section == 0) {
                selectedObject = self.planets[path.row];
            }else if (path.section == 1)
            {
                selectedObject = self.addedSpaceObjects[path.row];
            }
            
            targetViewController.spaceObject = selectedObject;
        }
    }
    if ([segue.destinationViewController isKindOfClass: [AWAddSpaceObjectViewController class]]) {
        AWAddSpaceObjectViewController *addSpaceObjectVC = segue.destinationViewController;
        addSpaceObjectVC.delegate = self;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - AWAddSpaceObjectViewController Delegate

-(void)didCancel
{
    NSLog(@"didCancel");
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addSpaceObejct:(AWSpaceObject *)spaceObject
{
    [self.addedSpaceObjects addObject:spaceObject];
    
    //Will save to NSUserDefaults here
    NSMutableArray *spaceObjectsPropertyLists = [[[NSUserDefaults standardUserDefaults] arrayForKey:ADDED_SPACE_OBJECT_KEY] mutableCopy];
    if (!spaceObjectsPropertyLists) spaceObjectsPropertyLists = [[NSMutableArray alloc]init];
                                                                 
                                                                 
    [spaceObjectsPropertyLists addObject:[self spaceObjectsAsAPropertyList:spaceObject]];
    [[NSUserDefaults standardUserDefaults] setObject:spaceObjectsPropertyLists forKey:ADDED_SPACE_OBJECT_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
                                                                 
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.tableView reloadData];
}

#pragma mark - Helper Methods

-(NSDictionary *)spaceObjectsAsAPropertyList: (AWSpaceObject *)spaceObject
{
    NSData *imageData = UIImagePNGRepresentation(spaceObject.spaceImage);
    NSDictionary *dictionary = @{PLANET_NAME: spaceObject.name,
                                 PLANET_GRAVITY:@(spaceObject.gravitationalForce),
                                 PLANET_DIAMETER:@(spaceObject.diameter),
                                 PLANET_YEAR_LENGTH:@(spaceObject.yearLength),
                                 PLANET_DAY_LENGTH:@(spaceObject.dayLength),
                                 PLANET_TEMPERATURE:@(spaceObject.temperature),
                                 PLANET_NUMBER_OF_MOONS:@(spaceObject.numberOfMoons),
                                 PLANET_NICKNAME:spaceObject.nickName,
                                 PLANET_INTERESTING_FACT:spaceObject.interestFact};
    return dictionary;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    if ([self.addedSpaceObjects count]) {
        return 2;
    }
    else
    {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if (section == 1) {
        return [self.addedSpaceObjects count];
    }
    else{
        return [self.planets count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"Cell" forIndexPath:(NSIndexPath*) indexPath];
    
    // Configure the cell...
    
    if (indexPath.section == 1) {
        //Use new SpaceObject to customize our cell
        AWSpaceObject *planet = [self.addedSpaceObjects objectAtIndex:indexPath.row];
        cell.textLabel.text = planet.name;
        cell.detailTextLabel.text = planet.nickName;
        cell.imageView.image = planet.spaceImage;
    }
    else{
        /* Acces the AWSpaceObject from our planets array. Use the AWSpaceObject's properties to update the cell's properties. */
        AWSpaceObject *planet = [self.planets objectAtIndex:indexPath.row];
        cell.textLabel.text = planet.name;
        cell.detailTextLabel.text = planet.nickName;
        cell.imageView.image = planet.spaceImage;
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"push to space data" sender:indexPath];
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) return YES;
    else return NO;
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.addedSpaceObjects removeObjectAtIndex:indexPath.row];
        
        NSMutableArray *newSavedSpaceObject = [[NSMutableArray alloc]init];
        
        for (AWSpaceObject *spaceObject in self.addedSpaceObjects) {
            [newSavedSpaceObject addObject:[self spaceObjectsAsAPropertyList:spaceObject]];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:newSavedSpaceObject forKey:ADDED_SPACE_OBJECT_KEY];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
