//
//  AWSpaceObject.h
//  UITableViewChallengeSolution
//
//  Created by Adrian Watzlawczyk on 05/09/14.
//  Copyright (c) 2014 ___ADRIANWATZLAWCZYK___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AWSpaceObject : NSObject

@property (strong,nonatomic) NSString *name;
@property (nonatomic) float gravitationalForce;
@property (nonatomic) float diameter;
@property   (nonatomic) float yearLength;
@property   (nonatomic) float dayLength;
@property   (nonatomic) float temperature;
@property   (nonatomic) int numberOfMoons;
@property   (strong, nonatomic ) NSString *nickName;
@property   (strong,nonatomic) NSString *interestFact;
@property   (strong, nonatomic) UIImage *spaceImage;

-(id) initWithData: (NSDictionary *)data andImage:(UIImage *)image;

@end
