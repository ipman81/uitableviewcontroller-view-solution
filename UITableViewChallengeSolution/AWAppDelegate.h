//
//  AWAppDelegate.h
//  UITableViewChallengeSolution
//
//  Created by Adrian Watzlawczyk on 05/09/14.
//  Copyright (c) 2014 ___ADRIANWATZLAWCZYK___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
