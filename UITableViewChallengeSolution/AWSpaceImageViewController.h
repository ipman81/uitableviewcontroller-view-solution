//
//  AWSpaceImageViewController.h
//  UITableViewChallengeSolution
//
//  Created by Adrian Watzlawczyk on 05/09/14.
//  Copyright (c) 2014 ___ADRIANWATZLAWCZYK___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSpaceObject.h"

@interface AWSpaceImageViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,nonatomic) UIImageView *imageView;
@property (strong,nonatomic) AWSpaceObject *spaceObject;

@end
