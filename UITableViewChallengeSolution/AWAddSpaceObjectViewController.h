//
//  AWAddSpaceObjectViewController.h
//  UITableViewChallengeSolution
//
//  Created by Adrian Watzlawczyk on 11/09/14.
//  Copyright (c) 2014 ___ADRIANWATZLAWCZYK___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSpaceObject.h"

@protocol AWAddSpaceObjectViewControllerDelegate <NSObject>

@required

-(void)addSpaceObejct: (AWSpaceObject *)spaceObject;
-(void)didCancel;

@end

@interface AWAddSpaceObjectViewController : UIViewController

@property (weak,nonatomic) id<AWAddSpaceObjectViewControllerDelegate>delegate;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *nickNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *diameterTextField;
@property (strong, nonatomic) IBOutlet UITextField *temperatureTextField;
@property (strong, nonatomic) IBOutlet UITextField *numberOfMoonsTextField;
@property (strong, nonatomic) IBOutlet UITextField *interestingFactTextField;
- (IBAction)cancelButtonPressed:(UIButton *)sender;
- (IBAction)addButtonPressed:(UIButton *)sender;

@end
