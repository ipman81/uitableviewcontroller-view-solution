//
//  AWSpaceDataViewController.h
//  UITableViewChallengeSolution
//
//  Created by Adrian Watzlawczyk on 08/09/14.
//  Copyright (c) 2014 ___ADRIANWATZLAWCZYK___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSpaceObject.h"

@interface AWSpaceDataViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) AWSpaceObject *spaceObject;

@end
